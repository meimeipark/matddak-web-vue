// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    pageTransition: { name: "page", mode: "out-in" },
  },
  css: ["~/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  typescript: {
    shim: false,
    strict: true,
  },
  runtimeConfig: {
    apiSecret: "",
    public: {
      apiBase: "/api",
    },
  },
  modules: [
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "@kevinmarrec/nuxt-pwa",
    "@vueuse/nuxt",
    '@element-plus/nuxt',
    'nuxt-3-axios',
  ],
  pwa: {
    workbox: {
      enabled: false,
    },
  },
  build: {
    // Transpile ESM modules because Nuxt 2 uses CommonJS for building server and `vue-chart-3` uses `lodash-es` internaly
    transpile: ['lodash-es', 'vue-chart-3'],
  },
  axios:{
    baseURL: 'http://matddak.shop:8080/v1'
    //options to pass Axios Config
  }
});
