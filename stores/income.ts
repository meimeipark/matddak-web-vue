import {defineStore} from 'pinia';

// interface IIncome {
//   delivery: number
//   feeRider: number
//   feeAdmin: number
//   feeTotal: number
//   taxSan: number
//   taxGo: number
//   dateIncome: string
//   bossName: string
//   timeIncome: string
// }

export const useIncomeStore = defineStore('income', {
    state: () => ({
        list: [],
        staticsData: [],
        totalCount: 0,
        totalPage: 0,
        currentPage: 0
    }),
    actions: {
        async fetchIncome(pageNum: number) {
            const token = useCookie('token');
            // const {pageNum} = useRoute().params
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/income/all/${pageNum}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                });
            if (data) {
                this.list = data.value.list;
                this.totalCount = data.value.totalCount;
                this.totalPage = data.value.totalPage;
                this.currentPage = data.value.currentPage;
            }
        },
        async fetchStatics() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/income/statistics/admin`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                });
            if (data.value) {
                this.staticsData = data.value.data;
            }
        },
        async fetchDetail(incomeId: string) {
            const {id} = useRoute().params;
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/income/all/${id}`, {
                    method: 'GET',
                },
            )
        }
    }
})
if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useIncomeStore, import.meta.hot))
}
