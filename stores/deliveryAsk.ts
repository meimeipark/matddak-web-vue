import {defineStore} from 'pinia';
import type {INotice} from "~/interface";

interface deliveryAskRequest{
  id : number
  dateAsk : string // 주문일
  storeName : string // 업소명
  addressStoreDo : string // 가게주소
  addressClientDo : string // 고객주소
  askMenu : string // 주문메뉴
  requestStore : string // 가게 요청사항
  requestRider : string // 라이더 요청사항
  priceTotal : number // 결제 금액
  priceFood : number // 주문가격
  feeTotal : number // 배달비
}

// board 스토어
export const useNoticeAskDelivery = defineStore('delivery', {
  state: () => ({
    askDeliverys:[],
    askDeliverym:{},
    totalCount: 0,
    totalPage: 0,
    currentPage:0,
  }),
  actions: {
    async fetchCancelDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/ask/all/cancel/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.askDeliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchRequestDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/ask/all/request/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.askDeliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchNotice(boardId:number) {
      // const token = useCookie('token');
      const { id} = useRoute().params
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/ask/detail/${id}`, {
          method: 'GET',
          // headers: {
          //   'Authorization': `Bearer ${token}`
          // },
        },
      );
      console.log(data);
      if (data.value) {
        this.askDeliverym = data.value.data;
      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useNoticeAskDelivery, import.meta.hot))
}
