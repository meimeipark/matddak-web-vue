import {defineStore} from 'pinia';
import type {INotice} from "~/interface";


// board 스토어
export const useNoticeStore = defineStore('boardList', {
  state: () => ({
    boards: [] as INotice[],
    board: {},
    totalCount: 0,
    totalPage: 0,
    currentPage: 0
  }),
  actions: {
    async setBoard(data: INotice) {
      const token = useCookie('token');
      const {title, text} = data;
      const noticeForm = new FormData();
      noticeForm.append('title', title);
      noticeForm.append('text', text);
      // if (multipartFile) {
      //   noticeForm.append('multipartFile', multipartFile);
      // }
      useFetch('http://matddak.shop:8080/v1/board/new', {
        method: 'POST',
        body: noticeForm,
        headers: {
          'Authorization': `Bearer ${token}`
        },
      })
    },
    // async setBoard(payload:INotice) {
    //   const token = useCookie('token');
    //   const {title, text, multipartFile} = payload;
    //
    //   return new Promise((resolve, reject) => {
    //     const noticeForm = new FormData();
    //     noticeForm.append('title', title);
    //     noticeForm.append('text', text);
    //     if (multipartFile) {
    //       noticeForm.append('multipartFile', multipartFile );
    //     }
    //     useFetch('http://matddak.shop:8080/v1/board/new', {
    //       method: 'POST',
    //       body: noticeForm,
    //       headers: {
    //         'Content-Type': 'multipart/form-data',
    //         'Authorization': `Bearer ${token}`
    //       },
    //     })
    //
    //   })
    //
    // },
    async putBoard(data: INotice) {
      const {id} = useRoute().params;
      const {title, text, multipartFile} = data;
      const boardForm = new FormData();
      boardForm.append('title', title);
      boardForm.append('text', text);
      if (multipartFile) {
        boardForm.append('multipartFile', multipartFile);
      }
      console.log("put1");
      useFetch(`http://matddak.shop:8080/v1/board/change/${id}`, {
        method: 'PUT',
        body: boardForm,
      });
      console.log("put2");
    },
    async fetchNoticeList(pageNum: number) {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/board/all/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data.value) {
        this.boards = data.value.list;
        this.currentPage = data.value.currentPage;
        this.totalPage = data.value.totalPage;
        console.log(data.value.list)
        console.log(data.value.currentPage)
      }
    },

    async fetchNotice(boardId: string) {
      const token = useCookie('token');
      const {id} = useRoute().params;
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/board/detail/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.board = data.value.data;
        // console.log(data.value.data.id)
      }
    },

    async delNotice(boardId: number) {
      const token = useCookie('token');
      const {id} = useRoute().params
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/board/delete/${id}`, {
          method: 'DELETE',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data.value) {
        this.boards = data.value.data;
      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
