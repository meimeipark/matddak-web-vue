import {defineStore} from 'pinia';

// auth 스토어
export const useHistoryStore = defineStore('history', {
  state: () => ({
    moneyList: [],
    money: {},
    totalCount: 0,
    totalPage: 0,
    currentPage: 0
  }),
  actions: {
    async fetchHistoryList(pageNum: number) {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/all/out/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data.value) {
        this.moneyList = data.value.list;
        this.totalCount = data.value.totalCount;
        this.totalPage = data.value.totalPage;
        this.currentPage = data.value.currentPage;
        console.log(data);
      }
    },
    async fetchHistory(historyId: number) {
      const token=useCookie('token');
      const {id} = useRoute().params;
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/detail/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data) {
        this.money=data.value.data;
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useHistoryStore, import.meta.hot))
}
