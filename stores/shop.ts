import {defineStore} from 'pinia';

interface IShopCreate {
  storeName: string
  addressStoreDo: string
  addressStoreG: string
  storeNumber: string
  sellTime: string
  holiday: string
  deliveryArea: string
  bossName: string
  shopId: string
  storeX: number
  storeY: number
}

interface IStore {
  storeName: string
  addressStoreDo: string
  addressStoreG: string
  storeNumber: string
  sellTime: string
  holiday: string
  deliveryArea: string
  bossName: string
  shopId: string
  storeX: number
  storeY: number
}


// auth 스토어
export const useShopStore = defineStore('shop', {
  state: () => ({
    list: [],
    shops: {},
    totalCount: 0,
    totalPage: 0,
    currentPage: 0
  }),
  actions: {
    setShop(data: IShopCreate) {
      useFetch('http://matddak.shop:8080/v1/store/new', {
        method: 'POST',
        body: data,
      })
    },
    async fetchShopList(pageNum:number) {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/store/all/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data.value) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;
        console.log(data);
      }
    },
    async fetchShop(storeId: number) {
      const token=useCookie('token');
      const {id} = useRoute().params;
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/store/detail/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        }
      );
      if (data) {
        this.shops=data.value.data;
      }
    },

    async delShop(storeId: string) {
      const token = useCookie('token');
      const {id} = useRoute().params
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/store/delete/${id}`, {
          method: 'DELETE',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data.value) {
        this.list = data.value.data;
      }
    }

  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useShopStore, import.meta.hot))
}
